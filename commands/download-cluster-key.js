'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function downloadclusterkey(user_token){
	return decryptToken(user_token)
		.then(user => readKey(user.username));
}

module.exports = downloadclusterkey;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readKey = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.ssh/id_rsa`, 'utf8', function(err, data) {
		if(err)
			reject();
		else {
			resolve(data);
		}
	});
});
