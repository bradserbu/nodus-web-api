'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function getnotifications(user_token){
	return decryptToken(user_token)
		.then(user => readNotifications(user.username));
}

module.exports = getnotifications;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readNotifications = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/notification_history.json`, function(err, data) {
                if(err)
                        throw err;

                var notifications = JSON.parse(data).notifications;
                resolve(notifications);
        });
});
