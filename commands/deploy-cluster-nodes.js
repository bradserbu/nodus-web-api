'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const run = require('./run-shell-command');
const deployAzureNode = ('./scripts/deploy-azure-node.sh');

function deployclusternodes(user_token, node_count)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			for(var i = 0; i < node_count; i++) {
				run(deployAzureNode, [username]);
			}

			return;
		});
}

module.exports = deployclusternodes;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});
