'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function addshortcutitem(user_token, job){
	return decryptToken(user_token)
		.then(user => addShortcut(user.username, job));
}

module.exports = addshortcutitem;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const addShortcut = (username, job) => new Promise(function(resolve, reject) {
	let shortcut_items = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/shortcut_list.json`));
	shortcut_items.shortcuts.push(job);
	
	fs.writeFile(`/home/${username}/.nodus/shortcut_list.json`, JSON.stringify(shortcut_items), function(err, data) {
		if(err)
			throw err;
		else
			resolve(job);
	});
});
