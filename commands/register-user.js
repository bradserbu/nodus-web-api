'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const sendConfirmationEmail = require('./send-confirmation-email');

const addUserToSystem = require('./create-user-on-system');


function registeruser(username, name, email, company, password) {
	return createUserToken(username, name, email, company)
		.then(result => {
			const token = result;
			return addUserToSystem(username, password)
				.then(_ => saveUserToken(username, token))
				.then(_ => initializeUserLogging(username))
				//.then(_ => sendConfirmationEmail(name, email))
				.then(_ => token);
		});
}

module.exports = registeruser;


const createUserToken = (username, name, email, company) => new Promise(function(resolve, reject) {
        resolve(jwt.sign({
                        username: username,
                        name: name,
			email: email,
                        company: company,
			avatar: 'assets/images/avatars/profile.jpg'
                }, config.app_secret)
        );
});

function saveUserToken(username, user_token) {
	fs.writeFile(`/home/${username}/.nodus/token`, user_token.toString(), 'utf8', function(err) {
                if(err)
                        throw err;
        });
}

function initializeUserLogging(username) {
	const timestamp = new Date().toLocaleString();
	const logActivity = {
		'time': timestamp,
		'title': {
			'name': `User account ${username}`,
			'icon': 'assets/images/avatars/profile.jpg'
		},
		'message': ' was created on the system.'
	};
	
	fs.writeFile(`/home/${username}/.nodus/activities_history`, JSON.stringify(logActivity) + '\n' , function(err) {
		if(err)
			throw err;
	});	
}
