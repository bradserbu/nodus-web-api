'use strict'

const CMD = "./scripts/validate-credentials.sh";
const run = require('./run-shell-command');
const fs = require('fs');

function login(username, password) {
	return run(CMD, [username, password])
		.then(_ => getToken(username));
}

module.exports = login;

function getToken(username) {
	return fs.readFileSync(`/home/${username}/.nodus/token`, 'utf8');
}
