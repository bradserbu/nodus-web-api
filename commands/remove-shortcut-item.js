'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function removeshortcutitem(user_token, item_index){
	return decryptToken(user_token)
		.then(user => removeShortcut(user.username, item_index));
}

module.exports = removeshortcutitem;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const removeShortcut = (username, item_index) => new Promise(function(resolve, reject) {
	let shortcut_items = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/shortcut_list.json`));
	shortcut_items.shortcuts.splice(item_index, 1);
	
	fs.writeFile(`/home/${username}/.nodus/shortcut_list.json`, JSON.stringify(shortcut_items), function(err, data) {
		if(err)
			throw err;
		else
			resolve();
	});
});
