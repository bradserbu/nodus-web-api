'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function getburstingconfiguration(user_token){
	return decryptToken(user_token)
		.then(user => readConfiguration(user.username));
}

module.exports = getburstingconfiguration;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readConfiguration = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/bursting_config.json`, function(err, data) {
		if(err)
			resolve({});
		else
			resolve(JSON.parse(data));
	});
});
