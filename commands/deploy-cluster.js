'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const deployCluster = "./scripts/deploy-cluster.sh";
const getClusterIP = "./scripts/get-cluster-ip.sh";
const clusterOnline = "./scripts/cluster-online.sh";
const run = require('./run-shell-command');
const logActivity = require('./log-activity');

function deploycluster(provider_id, provider_label, user_token, username, cluster_id, cluster_name, on_demand, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, region)  {
	return addToClusterConfig(provider_label, provider_id, username, cluster_id, cluster_name, on_demand, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, region)
		.then(_ => run(deployCluster, [provider_id, username, cluster_id, cluster_name, cluster_size, head_node_size, compute_node_size]))
		.then(result => {
			return run(getClusterIP, [provider_id, username, cluster_id])
				.then(cluster_ip => updateClusterState(username, cluster_id, cluster_ip.stdout, ""))
				.then(_ => run(clusterOnline, [provider_id, username, cluster_id]))
				.then(_ => updateClusterState(username, cluster_id, "", "Online"))
				.then(cluster => {
					 const activity = {
                                         	'type': 'cloud_provider',
                                                'title': {
                                                	'name': cluster_name,
                                                        'icon': `/assets/images/providers/${provider_label}.png`,
                                                },
                                                'message': ' was deployed.'
                                         };
                                         return logActivity(user_token, activity)
						.then(_ => cluster);	
				});
		});
}

module.exports = deploycluster;

const addToClusterConfig = (provider_label, provider_id, username, cluster_id, cluster_name, on_demand, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, region) => new Promise(function(resolve, reject) {
	const cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));
	const timestamp = new Date().toLocaleString();

	const cluster =               	{
        	"id": cluster_id,
		"created_at": timestamp,
		"provider": provider_label,
		"provider_id": provider_id,
                "name": cluster_name,
		"icon": `assets/images/providers/${provider_label}.png`,
		"state": "Provisioning",
		"size": cluster_size,
		"head_node_size": head_node_size,
		"head_node_label": head_node_label,
		"compute_node_size": compute_node_size,
		"compute_node_label": compute_node_label,
                "api": "",
		"on_demand": on_demand,
		"idle_purge_time": cluster_purge_time,
		"move_data": move_data,
		"region": region
	};

	cluster_config.clusters.push(cluster);

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
		if(err)
			throw err;

		resolve();
	});
});

const updateClusterState = (username, cluster_id, cluster_ip, cluster_state) => new Promise(function(resolve, reject) {
        const cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));
	let cluster = {};	

        for(let i = 0; i < cluster_config.clusters.length; i++) {
                if(cluster_config.clusters[i].id == cluster_id) {
			if(cluster_state == 'Online') {
                        	cluster_config.clusters[i].state = cluster_state;
			} else {
				if (cluster_ip) {
					cluster_config.clusters[i].api = `http://${cluster_ip}:12001/api`;
				} else {
					cluster_config.clusters[i].state = "Failed";
				}
			}
			cluster = cluster_config.clusters[i];
                        break;
                }
        } 

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;

                resolve(cluster);
        });
}); 
