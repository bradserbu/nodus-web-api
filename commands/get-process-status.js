'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');

function getprocessstatus(user_token, process_id){
	return decryptToken(user_token)
		.then(user => getProcess(user.username, process_id));
}

module.exports = getprocessstatus;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const getProcess = (username, process_id) => new Promise(function(resolve, reject) {
	const process = {
		'id': 'qwert',
		'title': 'Deploying Node',
		'tasks': [
			{
				'id': 'deploynode_1',
				'title': 'Creating Instance'
			},
			{
				'id': 'deploynode_2',
				'title': 'Installing Pre-Build Image'
			},
			{
				'id': 'deploynode_3',
				'title': 'Installing Torque'
			},
			{
				'id': 'deploynode_4',
				'title': 'Installing NODUS'
			},
			{
				'id': 'deploynode_5',
				'title': 'Staring NODUS Torque Services'
			}
		]
	};

	resolve(process);
});
