'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const redeployCluster = "./scripts/redeploy-cluster.sh";
const getClusterIP = "./scripts/get-cluster-ip.sh";
const clusterOnline = "./scripts/cluster-online.sh";
const run = require('./run-shell-command');
const logActivity = require('./log-activity');

function redeploycluster(user_token, provider_id, cluster_id)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return updateClusterState(username, cluster_id, "", "Redeploying")
				.then(_ => run(redeployCluster, [provider_id, username, cluster_id]))
				.then(result => {
                        		return run(getClusterIP, [provider_id, username, cluster_id])
                                		.then(cluster_ip => updateClusterState(username, cluster_id, cluster_ip.stdout, ""))
                                		.then(_ => run(clusterOnline, [provider_id, username, cluster_id]))
                                		.then(_ => updateClusterState(username, cluster_id, "", "Online"))
                                		.then(cluster => {
                                         		const activity = {
                                                		'type': 'cloud_provider',
                                                		'title': {
                                                        		'name': cluster_name,
                                                        		'icon': `/assets/images/providers/${cluster.provider}.png`,
                                                		},
                                                		'message': ' was redeployed.'
                                         		};
                                         		return logActivity(user_token, activity)
                                                		.then(_ => cluster);
						});
                                });
                });					
}

module.exports = redeploycluster;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const updateClusterState = (username, cluster_id, cluster_ip, cluster_state) => new Promise(function(resolve, reject) {
        const cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));
        let cluster = {};

        for(let i = 0; i < cluster_config.clusters.length; i++) {
                if(cluster_config.clusters[i].id == cluster_id) {
			if(cluster_state == 'Redeploying') {
				cluster_config.clusters[i].state = "Redeploying";
			} else if(cluster_state == 'Online') {
                                cluster_config.clusters[i].state = "Online";
                        } else {
                                if (cluster_ip) {
                                        cluster_config.clusters[i].api = `http://${cluster_ip}:12001/api`;
                                } else {
                                        cluster_config.clusters[i].state = "Failed";
                                }
                        }
                        cluster = cluster_config.clusters[i];
                        break;
                }
        }

        fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;

                resolve(cluster);
        });
});
