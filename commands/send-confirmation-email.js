'use strict'

const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require('./../config.json');

function sendconfirmationemail(name, email_address) {
	var content = `
		<h3>Hello ${name}!</h3>
		<p>Thank you for registering and welcome to NODUS 3.0! We have received your data and created an account for you. To get started, simply visit:
		<br>
		<br>
		https://www.nodusplatform.com
		<br>
		<br>
		and login in with your username and password.
		<br>
		<br>
		</p>
		Sincerely,
		<br>
		Adaptive Computing`;

	var transporter = nodemailer.createTransport(smtpTransport({
		service: '',
		port: 25,
		secure: false,
		auth: {
			user: '',
			pass: config.email_password
		},
		tls: {
			rejectUnauthorized: false
		}
	}));

	var mailOptions = {
		from: '',
		to: '' + email_address,
		subject: 'NODUS 3.0 Registration',
		html: content
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if(error)
			console.log(error);
	});
}

module.exports = sendconfirmationemail;
