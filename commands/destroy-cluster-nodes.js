'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const run = require('./run-shell-command');
const destroyAzureNode = ('./scripts/remove-azure-node.sh');

function destroyclusternodes(user_token, node_count)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			for(var i = 0; i < node_count; i++) {
				run(destroyAzureNode, [username]);
			}
	
			return;
		});
}

module.exports = destroyclusternodes;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});
