'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const run = require('./run-shell-command');
const removeNode = ('./scripts/remove-azure-node.sh');

function removeazurenode(user_token, node_id)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return run(removeNode, [username, node_id]);
		});
}

module.exports = removeazurenode;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});
