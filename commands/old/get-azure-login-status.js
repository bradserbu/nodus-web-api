'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');

function getazureloginstatus(user_token)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return readLoginStatus(username);
		});
}

module.exports = getazureloginstatus;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readLoginStatus = (username) => new Promise(function(resolve, reject) {
	try {
  		let cluster = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster.json`));
		resolve(cluster.azure.logged_in);
	} catch (err) {
  		resolve(false);
	}	
});
