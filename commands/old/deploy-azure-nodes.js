'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const run = require('./run-shell-command');
const deployNode = ('./scripts/deploy-azure-node.sh');

function deployazurenodes(user_token)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return run(deployNode, [username]);
		});
}

module.exports = deployazurenodes;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});
