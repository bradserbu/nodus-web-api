'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const deployCluster = "./scripts/deploy-cluster.sh";
const getClusterIP = "./scripts/get-cluster-ip.sh";
const run = require('./run-shell-command');
const logActivity = require('./log-activity');

function deploycluster(user_token, provider_id, provider_label, username, cluster_id, cluster_name, on_demand, cluster_size, cluster_purge_time, move_data)  {
	console.log("DEPLOYING Google!!");
	return addToClusterConfig(provider_label, username, cluster_id, cluster_name, on_demand, cluster_size, cluster_purge_time, move_data)
		.then(_ => run(deployGoogleCluster, [provider_id, username, cluster_id, cluster_name, cluster_size]))
		.then(result => {
			return run(getClusterIP, [provider_id, username, cluster_id])
				.then(cluster_ip => updateClusterState(username, cluster_id, cluster_ip.stdout))
				.then(cluster => {
					 const activity = {
                                         	'type': 'cloud_provider',
                                                'title': {
                                                	'name': cluster_name,
                                                        'icon': `/assets/images/providers/${provider_label}.png`,
                                                },
                                                'message': ' was deployed.'
                                         };
                                         return logActivity(user_token, activity)
						.then(_ => cluster);	
				});
		});
}

module.exports = deploycluster;

const addToClusterConfig = (provider_label, username, cluster_id, cluster_name, on_demand, cluster_size, cluster_purge_time, move_data) => new Promise(function(resolve, reject) {
	const cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));
	const timestamp = new Date().toLocaleString();

	const cluster =               	{
        	"id": cluster_id,
		"created_at": timestamp,
		"provider": provider_label,
                "name": cluster_name,
		"icon": `assets/images/providers/${provider_label}.png`,
		"state": "Provisioning",
		"size": cluster_size,
                "api": "",
		"on_demand": on_demand,
		"idle_purge_time": cluster_purge_time,
		"move_data": move_data
	};

	cluster_config.clusters.push(cluster);

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
		if(err)
			throw err;

		resolve();
	});
});

const updateClusterState = (username, cluster_id, cluster_ip) => new Promise(function(resolve, reject) {
        const cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));
	let cluster = {};	

        for(let i = 0; i < cluster_config.clusters.length; i++) {
                if(cluster_config.clusters[i].id == cluster_id) {
                        cluster_config.clusters[i].state = "Online";
			cluster_config.clusters[i].api = `http://${cluster_ip}:12001/api`;
			cluster = cluster_config.clusters[i];
                        break;
                }
        } 

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;

                resolve(cluster);
        });
}); 
