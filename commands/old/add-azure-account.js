'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const loginAzure = "./scripts/login-azure.sh";
const spawn = require('child_process').spawn;

function addazureaccount(user_token)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return spawnProcess(username);
		});
}

module.exports = addazureaccount;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const spawnProcess = (username) => new Promise(function(resolve, reject) {
        var ls = spawn(loginAzure, [username]);

        ls.stderr.on('data', function(data) {
		var code = data.toString().split("code ")[1].split(" ")[0];
                resolve(code);
        });

	ls.on('close', (code) => {
		if(code == 0) {
  			fs.writeFile(`/home/${username}/.nodus/cluster.json`, '{"azure": {"logged_in": true}}', function(err) {
                		if(err)
                        		throw err;
        		});	
		}
	});
});
