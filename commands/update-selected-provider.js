'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const request = require('./make-http-request');
const fs = require('fs');

function updateselectedprovider(user_token, provider) {
	return decryptToken(user_token)
		.then(user => updateBurstingConfiguration(user.username, provider));
}

module.exports = updateselectedprovider;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const updateBurstingConfiguration = (username, provider) => new Promise(function(resolve, reject) {
	let bursting_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/bursting_config.json`));
	bursting_config.selected_provider = provider;

        fs.writeFile(`/home/${username}/.nodus/bursting_config.json`, JSON.stringify(bursting_config), function(err) {
                if(err)
                        throw err;
		resolve();
        });

	let cluster_config = JSON.parse(fs.readfileSync(`/home/${username}/.nodus/cluster_config.json`));
	cluster_config.selected_cluster = provider;

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;
                resolve();
        });	
	
});
