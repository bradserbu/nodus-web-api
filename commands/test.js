'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const deployAzureCluster = "./scripts/deploy-azure-cluster.sh";
const getClusterIP = "./scripts/get-cluster-ip.sh";
const run = require('./run-shell-command');

function deployazurecluster(username)  {
					console.log("-------------------------------------");
					console.log("CLUSTER DEPLOYED!");
					console.log("-------------------------------------");
					console.log("-------------------------------------");
					return run(getClusterIP, [username])
						.then(cluster_ip => {
							console.log(cluster_ip);	
							return saveClusterConfig(username, cluster_ip)
								.then(_ => updateConfiguredProviders(username, 'Microsoft Azure', 'assets/images/providers/Microsoft Azure.png', 'https://azure.microsoft.com'));
						});
}

module.exports = deployazurecluster;


const saveClusterConfig = (username, cluster_ip) => new Promise(function(resolve, reject) {
	let cluster = {
        	"selected_cluster": "sfgn24f287",
        	"clusters": [
                	{
                        	"id": "sfgn24f287",
                        	"name": "Microsoft Azure",
                        	"api": `http://${cluster_ip}:12001/api`
                	}
        	]
	};

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster), function(err) {
		if(err)
			throw err;

		resolve();
	});
});

function updateConfiguredProviders(username, name, icon, link) {
	let configured_providers = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/bursting_config.json`));
	const timestamp = new Date().toLocaleString();
	
	const new_provider = {
		'name': name, 
		'icon': icon,
		'link': link,
		'date': timestamp
	};
	
	configured_providers.selected_provider = name;

	let previously_configured = false;
	for(let i = 0; i < configured_providers.providers.length; i++) {
		if(configured_providers.providers[i].name == new_provider.name) {
			configured_providers.providers[i].date = new_provider.date;
			previously_configured = true;	
		}
	}

	if(!previously_configured)
		configured_providers.providers.push(new_provider);	
	
	fs.writeFile(`/home/${username}/.nodus/bursting_config.json`, JSON.stringify(configured_providers), function(err) {
		if(err)
			throw err;
	});
}
