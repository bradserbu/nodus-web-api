'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');

function addactiveprocess(user_token, id, name, tasks)  {
	return decryptToken(user_token)
		.then(user => updateActiveProcesses(user.username, id, name, tasks));
}

module.exports = addactiveprocess;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

function updateActiveProcesses(username, id, name, tasks) {
	let active_processes = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/active_processes.json`));
	
	const new_process = {
		'id': id,
		'name': name,
		'tasks': tasks,
		'state': 'active'
	};
	
	active_processes.processes.push(new_process);	
	
	fs.writeFile(`/home/${username}/.nodus/active_processes.json`, JSON.stringify(active_processes), function(err) {
		if(err)
			throw err;
	});
}
