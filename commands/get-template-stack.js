'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function gettemplatestacks(user_token){
	return decryptToken(user_token)
		.then(user => readStack(user.username));
}

module.exports = gettemplatestacks;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readStack = (username) => new Promise(function(resolve, reject) {
	var template_stacks = [];	


	fs.readFile(`/root/stack.json`, function(err, data) {
		if(err)
			resolve(template_stacks);
		else {
			const stack_1 = {
				"name": "Base Stack",
				"subtitle": "Default",
				"description": "This stack builds a Ubuntu machine with a torque client installed.",
				"value": "base_stack",
				"data": JSON.parse(data),
				"icons": [
						{
							"src": "assets/images/test-drive/ubuntu.png",
							"alt": "ubuntu"
						}
				]
			};

			template_stacks.push(stack_1);

			fs.readFile(`/root/stack_2.json`, function(err, data) {
				if(err)
					resolve(template_stacks);
				else {

					const stack_2 = {
						"name": "GPU Stack",
						"subtitle": "Custom - GPU Jobs",
						"description": "This stack builds a Ubuntu machine with a torque client installed. In addition, it configures the use of NVIDIA GPU computing.",
						"value": "gpu_stack",
						"data": JSON.parse(data),
						"icons": [
                                                		{
                                                        		"src": "assets/images/test-drive/ubuntu.png",
                                                        		"alt": "ubuntu"
                                                		},
								{
									"src": "assets/images/test-drive/nvidia.png",
									"alt": "nvidia"
								}
                                		]

					};

					template_stacks.push(stack_2);
					
					resolve(template_stacks);
				}
			});
		}
	});
});
