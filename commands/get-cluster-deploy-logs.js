'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const convertDeployLogsHtml = "./scripts/convert-logs-to-html.sh"
const fs = require('fs');
const run = require('./run-shell-command');

function getclusterdeploylogs(user_token, provider_id, cluster_id){
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return run(convertDeployLogsHtml, [username, provider_id, cluster_id])
				.then(_ => readLogs(username, provider_id, cluster_id));
		});
}

module.exports = getclusterdeploylogs;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readLogs = (username, provider_id, cluster_id) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/${provider_id}/${cluster_id}/deploy_logs_html`, 'utf8', function(err, data) {
		if(err)
			reject();
		else {
			resolve(data);
		}
	});
});
