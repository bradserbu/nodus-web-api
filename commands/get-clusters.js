'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function getclusters(user_token){
	return decryptToken(user_token)
		.then(user => readClusters(user.username));
}

module.exports = getclusters;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readClusters = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/cluster_config.json`, function(err, data) {
		if(err)
			resolve({});
		else
			resolve(JSON.parse(data).clusters);
	});
});
