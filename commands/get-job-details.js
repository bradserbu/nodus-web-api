'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const request = require('./make-http-request');
const fs = require('fs');

function getjobdetails(user_token, job_id) {
	return decryptToken(user_token)
		.then(result => {
			const username = result.username;

			return getJobCluster(username, job_id)
				.then(result => getClusterApi(username, result))
				.then(cluster => {
					const job_data = {
						'job_id': job_id
					};
			
					const postOptions = {
						method: 'post',
						body: job_data,
						json: true,
						url: `${cluster.api}/getJobDetails`
					};
			
					return request(postOptions);
				});
		});
}

module.exports = getjobdetails;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const getJobCluster = (username, job_id) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/jobs.json`, function(err, data) {
                if(err)
                        throw err;

                const jobs = JSON.parse(data).jobs;

                for(var i = 0; i < jobs.length; i++) {
                        if(jobs[i].job_id == job_id) {
                                resolve(jobs[i].submit_cluster);
                        }
                }
                reject("No Cluster Found for Job.");
        });
});

const getClusterApi = (username, cluster) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/cluster_config.json`, function(err, data) {
                if(err)
                        throw err;

                const clusters = JSON.parse(data).clusters;
                for(var i = 0; i < clusters.length; i++) {
                        if(clusters[i].id == cluster) {
                                resolve(clusters[i]);
                        }
                }
                reject("No Cluster Found for ID.");
        });
});
