'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function getstacks(user_token){
	return decryptToken(user_token)
		.then(user => readStacks(user.username));
}

module.exports = getstacks;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readStacks = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/stacks.json`, function(err, data) {
		if(err)
			resolve({});
		else
			resolve(JSON.parse(data).stacks);
	});
});
