'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const request = require('./make-http-request');
const fs = require('fs');

function updateselectedcluster(user_token, cluster) {
	return decryptToken(user_token)
		.then(user => updateClusterConfiguration(user.username, cluster));
}

module.exports = updateselectedcluster;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const updateClusterConfiguration = (username, cluster) => new Promise(function(resolve, reject) {
	let cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));
	cluster_config.selected_cluster = cluster;

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;
                resolve(cluster_config.selected_cluster);
        });	
	
});
