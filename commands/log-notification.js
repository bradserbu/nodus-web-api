'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function lognotification(user_token, notification){
	return decryptToken(user_token)
		.then(user => saveNotificationToLog(user.username, notification));
}

module.exports = lognotification;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const saveNotificationToLog = (username, notification) => new Promise(function(resolve, reject) {
	let notification_history = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/notification_history.json`));

	let isNewNotification = true;
	
	for(var i = 0; i < notification_history.notifications; i++) {
		if(notification_history.notifications[i].id == notification.id) {
			isNewNotification = false;
			break;
		}
	}
	
	if(isNewNotification)
        	notification_history.notifications.push(notification);

        fs.writeFile(`/home/${username}/.nodus/notification_history.json`, JSON.stringify(notification_history), function(err) {
                if(err)
                        throw err;

		resolve(notification_history.notifications);
        });
});
