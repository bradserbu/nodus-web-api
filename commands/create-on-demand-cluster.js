'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const deployCluster = require('./deploy-cluster');
const submitJob = require('./submit-job');
const fs = require('fs');
const uniqid = require('uniqid');

function createondemandcluster(user_token, cluster_name, cluster_purge_time, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, move_data, provider_config, job)  {
	return decryptToken(user_token)
		.then(result => {
			const local_username = result.username;
			// CHECK IF CLUSTER WITH NAME ALREADY EXISTS
                        if (fs.existsSync(`/home/${local_username}/.nodus/clusters/${cluster_name}`)) {
                                console.log("clustername already exists")
                        } else {

			const cluster_id = uniqid('cluster-');
			switch(provider_config.provider) {
				case "Amazon Web Services":
					return saveAWSCredentials(local_username, cluster_id, provider_config.aws_access_key, provider_config.aws_secret_key, provider_config.aws_region)
						.then(_ => deployCluster("aws", "Amazon Web Services", user_token, local_username, cluster_id, cluster_name, true, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, provider_config.aws_region))
						.then(cluster => submitJob(user_token, job, cluster));
					break;
				case "Google Cloud":
                                        return saveCredentials("google", local_username, cluster_id, provider_config.credentials)
                                                .then(_ => deployCluster("google", "Google Cloud", user_token, local_username, cluster_id, cluster_name, true, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, "us-east1"))
                                                .then(cluster => submitJob(user_token, job, cluster));
                                        break;
				case "Microsoft Azure":
                                        return saveCredentials("azure", local_username, cluster_id, provider_config.credentials)
                                                .then(_ => deployCluster("azure", "Microsoft Azure", user_token, local_username, cluster_id, cluster_name, true, cluster_size, haed_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, provider_config.credentials.region))
                                                .then(cluster => submitJob(user_token, job, cluster));
                                        break;
				case "Oracle Cloud":
                                        return saveOracleCredentials("oracle", local_username, cluster_id, provider_config.credentials)
                                                .then(_ => deployCluster("oracle", "Oracle Cloud", user_token, local_username, cluster_id, cluster_name, true, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, provider_config.credentials.region))
                                                .then(cluster => submitJob(user_token, job, cluster));
                                        break;
				case "Huawei Cloud":
                                        return saveCredentials("huawei", local_username, cluster_id, provider_config.credentials)
                                                .then(_ => deployCluster("huawei", "Huawei Cloud", user_token, local_username, cluster_id, cluster_name, false, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, provider_config.credentials.region))
						.then(cluster => submitJob(user_token, job, cluster));	
				case "Open Telekom Cloud":
                                        return saveCredentials("otc", local_username, cluster_id, provider_config.credentials)
                                                .then(_ => deployCluster("otc", "Open Telekom Cloud", user_token, local_username, cluster_id, cluster_name, false, cluster_size, head_node_size, head_node_label, compute_node_size, compute_node_label, cluster_purge_time, move_data, provider_config.credentials.region))
                                                .then(cluster => submitJob(user_token, job, cluster));
				default:
					return "sorry";
			}
			}
		});
}

module.exports = createondemandcluster;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const saveAWSCredentials = (username, cluster_id, aws_access_key, aws_secret_key, aws_region) => new Promise(function(resolve, reject) {
	let credentials = {
		"access_key": aws_access_key,
		"secret_key": aws_secret_key,
		"region": aws_region
	};

	if (!fs.existsSync(`/home/${username}/.nodus/aws/${cluster_id}`)){
                fs.mkdirSync(`/home/${username}/.nodus/aws/${cluster_id}`);
        }

	fs.writeFile(`/home/${username}/.nodus/aws/${cluster_id}/credentials.json`, JSON.stringify(credentials), function(err) {
		if(err)
			throw err;
		
		fs.writeFile(`/home/${username}/.nodus/aws/${cluster_id}/workload_nodes_history`, "", function(err) {
                        if(err)
                                throw err;
                        resolve();
                });
	});	
});

const saveOracleCredentials = (provider, username, cluster_id, credentials) => new Promise(function(resolve, reject) {
        if (!fs.existsSync(`/home/${username}/.nodus/${provider}/${cluster_id}`)) {
                fs.mkdirSync(`/home/${username}/.nodus/${provider}/${cluster_id}`);
        }

        fs.writeFile(`/home/${username}/.nodus/${provider}/${cluster_id}/credentials.json`, JSON.stringify(credentials), function(err) {
                if(err)
                        throw err;

                fs.writeFile(`/home/${username}/.nodus/${provider}/${cluster_id}/oci_api_key.pem`, credentials.api_key, function(err) {
                        if(err)
                                throw err;

                        fs.writeFile(`/home/${username}/.nodus/${provider}/${cluster_id}/workload_nodes_history`, "", function(err) {
                                if(err)
                                        throw err;
                                resolve();
                        });
                });
        });
});

const saveCredentials = (provider, username, cluster_id, credentials) => new Promise(function(resolve, reject) {
	if (!fs.existsSync(`/home/${username}/.nodus/${provider}/${cluster_id}`)) {
                fs.mkdirSync(`/home/${username}/.nodus/${provider}/${cluster_id}`);
        }

        fs.writeFile(`/home/${username}/.nodus/${provider}/${cluster_id}/credentials.json`, JSON.stringify(credentials), function(err) {
                if(err)
                        throw err;

                fs.writeFile(`/home/${username}/.nodus/${provider}/${cluster_id}/workload_nodes_history`, "", function(err) {
                        if(err)
                                throw err;
                        resolve();
                });
        });
});
