'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');

function updateprofile(user_token, name, lastName, company, jobTitle, email, phone) {
	return decryptToken(user_token)
		.then(result => {
			const username = result.username;
			return createUserToken(username, result.avatar, name, lastName, company, jobTitle, email, phone)
				.then(result => saveUserToken(username, result));
		});
}

module.exports = updateprofile;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const createUserToken = (username, avatar, name, lastName, company, jobTitle, email, phone) => new Promise(function(resolve, reject) {
        resolve(jwt.sign({
                        username: username,
			avatar: avatar,
                        name: name,
			lastName: lastName,
                        company: company,
			jobTitle: jobTitle,
			email: email,
			phone: phone
                }, config.app_secret, {
                        expiresIn: "30d"
                })
        );
});

const saveUserToken = (username, user_token) => new Promise(function(resolve, reject) {
	fs.writeFile(`/home/${username}/.nodus/token`, user_token.toString(), 'utf8', function(err) {
                if(err)
                        throw err;
		resolve(user_token);
        });
});
