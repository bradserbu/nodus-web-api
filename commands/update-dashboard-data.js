'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');
const request = require('./make-http-request');

function updatedashboard(user_token) {
        return decryptToken(user_token)
                .then(user => {
                        const username = user.username;
			
			return getCurrentCluster(username)
				.then(cluster => {
					if(!cluster.id) {
						var data = {
							'queue': {
								'jobs': [],
								'total': 0,
								'running': 0,
								'queued': 0,
								'blocked': 0
							},
							'workload_history': {},
							'nodes': {
								'nodes': [],
								'total': 0,
								'available': 0,
								'busy': 0,
								'down': 0
							}
						};

						return getEmptyWorkloadNodesHistory(username)
							.then(history => {
								data['workload_nodes_progression'] = history;
								return getActivities(username)
									.then(activities => {
										data['activities'] = activities;
										return getJobStatistics(username)
											.then(job_stats => {
												data['job_stats'] = job_stats;
												return getClusters(username)
													.then(clusters => getClusterStatistics(clusters))
													.then(cluster_stats => {
														data['cluster_stats'] = cluster_stats;
														return data;
													});
											});
									});
							});
					} else {
						const postOptions = {
							method: 'get',
							url: `${cluster.api}/getClusterWorkloadNodes`
						};

						return request(postOptions)
							.then(result => {
								var data = JSON.parse(result).data;
			                        		return getWorkloadNodesHistory(username, cluster.name)
                                                			.then(history => {
                                                				data['workload_nodes_progression'] = history;
                                                        			return getActivities(username)
                                                        				.then(activities => {
                                                        					data['activities'] = activities;
                                                        					return getJobStatistics(username)
                                                        						.then(job_stats => {
                                                        							data['job_stats'] = job_stats;
														return getClusters(username)
															.then(clusters => getClusterStatistics(clusters))
															.then(cluster_stats => {
																data['cluster_stats'] = cluster_stats;
                                                               									return data;
															});
													});
                                                					});
                                					});
							});
					}				
				});
                });
}

module.exports = updatedashboard;

const decryptToken = (token) => new Promise(function(resolve, reject) {
        resolve(jwt.verify(token, config.app_secret));
});

const getCurrentCluster = (username) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/cluster_config.json`, function(err, data) {
                if(err)
                        throw err;

                const cluster_config = JSON.parse(data);
                const selected_cluster = cluster_config.selected_cluster;

		resolve(selected_cluster);
        });
});

const getEmptyWorkloadNodesHistory = (username) => new Promise(function(resolve, reject) {
	var workload_data = [];
        var workload_running = [];
        var workload_queued = [];
        var workload_blocked = [];
        var nodes_data = [];
        var nodes_available = [];
        var nodes_busy = [];
        var nodes_down = [];
        var timestamp_labels = [];
        var recent_history = [];

                //recent_history.push({'label': 'Jobs in Queue', 'data': workload_data, 'fill': 'start'});
                //recent_history.push({'label': 'Nodes in Cluster', 'data': nodes_data, 'fill': 'start'});
        recent_history.push({'label': 'Running Jobs', 'data': workload_running, 'fill': 'start', 'stack': '1'});
        recent_history.push({'label': 'Queued Jobs', 'data': workload_queued, 'fill': 'start', 'stack': '1'});
        recent_history.push({'label': 'Blocked Jobs', 'data': workload_blocked, 'fill': 'start', 'stack': '1'});
        recent_history.push({'label': 'Available Nodes', 'data': nodes_available, 'fill': 'start', 'stack': '2'});
        recent_history.push({'label': 'Busy Nodes', 'data': nodes_busy, 'fill': 'start', 'stack': '2'});
        recent_history.push({'label': 'Down Nodes', 'data': nodes_down, 'fill': 'start', 'stack': '2'});

        resolve({'timestamps': timestamp_labels, 'data': recent_history});
});

const getWorkloadNodesHistory = (username, cluster_name) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/clusters/${cluster_name}/workload_nodes_history`, function(err, data) {
                if(err)
                        throw err;

                var data_array = data.toString().split("\n");
                var workload_data = [];
		var workload_running = [];
		var workload_queued = [];
		var workload_blocked = [];
                var nodes_data = [];
		var nodes_available = [];
		var nodes_busy = [];
		var nodes_down = [];
                var timestamp_labels = [];
                var recent_history = [];

                var history_length = 14;
                if(data_array.length < 14)
                        history_length = data_array.length;

                for(var i = data_array.length -history_length; i < data_array.length -1; i++) {
                        var current_data = JSON.parse(data_array[i]);
                        workload_data.push(current_data.workload_size);
			workload_running.push(current_data.workload_running);
			workload_queued.push(current_data.workload_queued);
			workload_blocked.push(current_data.workload_blocked);
                        nodes_data.push(current_data.nodes_size);
			nodes_available.push(current_data.nodes_available);
			nodes_busy.push(current_data.nodes_busy);
			nodes_down.push(current_data.nodes_down);
                        timestamp_labels.push(current_data.timestamp);
                }

                //recent_history.push({'label': 'Jobs in Queue', 'data': workload_data, 'fill': 'start'});
                //recent_history.push({'label': 'Nodes in Cluster', 'data': nodes_data, 'fill': 'start'});
		recent_history.push({'label': 'Running Jobs', 'data': workload_running, 'fill': 'start', 'stack': '1'});
		recent_history.push({'label': 'Queued Jobs', 'data': workload_queued, 'fill': 'start', 'stack': '1'});
		recent_history.push({'label': 'Blocked Jobs', 'data': workload_blocked, 'fill': 'start', 'stack': '1'});
		recent_history.push({'label': 'Available Nodes', 'data': nodes_available, 'fill': 'start', 'stack': '2'});
		recent_history.push({'label': 'Busy Nodes', 'data': nodes_busy, 'fill': 'start', 'stack': '2'});
		recent_history.push({'label': 'Down Nodes', 'data': nodes_down, 'fill': 'start', 'stack': '2'});

                resolve({'timestamps': timestamp_labels, 'data': recent_history});
        });
});

const getActivities = (username) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/activities_history`, function(err, data) {
                if(err)
                        throw err;

                var data_array = data.toString().split("\n");
                var activities = [];

                for(var i = data_array.length -2; i >= 0; i--)  {
                        activities.push(JSON.parse(data_array[i]));
                }

                resolve(activities);
        });
});

const getJobStatistics = (username) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/jobs.json`, function(err, data) {
                if(err)
                        throw err;

                const result = JSON.parse(data);

                const timestamp = new Date();
                var today = 0;
                var week = 0;
                var month = 0;
                var year = 0;


                for(var i = 0; i < result.jobs.length; i++) {
                        const job_date = new Date(result.jobs[i].submit_time);
                        if(sameDay(job_date, timestamp)) {
                                today++;
                                week++;
                                month++;
                                year++;
                        } else if(sameWeek(job_date, timestamp)) {
                                week++;
                                month++;
                                year++;
                        } else if(sameMonth(job_date, timestamp)) {
                                month++;
                                year++;
                        } else if(sameYear(job_date, timestamp)) {
                                year++;
                        }
                }


                const job_stats = {
                        'total': result.jobs.length,
                        'today': today,
                        'week': week,
                        'month': month,
                        'year': year
                };

                resolve(job_stats);
        });
});

const getClusters = (username) => new Promise(function(resolve, reject) {
        fs.readFile(`/home/${username}/.nodus/cluster_config.json`, function(err, data) {
                if(err)
                        throw err;

                const clusters = JSON.parse(data).clusters;

                resolve(clusters);
        });
});

const getClusterStatistics = (clusters) => new Promise(function(resolve, reject) {
	let aws = 0;
	let google = 0;
	let azure = 0;
	let oracle = 0;

	let online = 0;

	for(var i = 0; i < clusters.length; i++) {
		switch(clusters[i].provider) {
			case "Amazon Web Services":
				aws++;
				break;
			case "Google Cloud":
				google++;
				break;
			case "Microsoft Azure":
				azure++;
				break;
			case "Oracle Cloud":
				oracle++;
				break;
			default:
				break;
		}

		if(clusters[i].state == "Online")
			online++;
	}

	const cluster_stats = {
		'aws': aws,
		'google': google,
		'azure': azure,
		'oracle': oracle,
		'online': online,
		'total': clusters.length
	};

	resolve(cluster_stats);
});

function sameDay(d1, d2) {
        return d1.getFullYear() === d2.getFullYear() &&
                d1.getMonth() === d2.getMonth() &&
                d1.getDate() === d2.getDate();
}

function sameWeek(d1, d2) {
//      d2.setDate(d2.getDate() -7);
        return false;
}

function sameMonth(d1, d2) {
        return d1.getFullYear() === d2.getFullYear() &&
                d1.getMonth() === d2.getMonth();
}

function sameYear(d1, d2) {
        return d1.getFullYear() === d2.getFullYear();
}
