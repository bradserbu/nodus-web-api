'use strict'

const CMD = "./scripts/add-system-user.sh";
const run = require('./run-shell-command');

function createsystemuser(username, password) {
	return run(CMD,  [username, password]);
}

module.exports = createsystemuser;
