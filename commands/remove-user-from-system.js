'use strict'

const CMD = "./scripts/remove-system-user.sh";
const run = require('./run-shell-command');

function removesystemuser(username) {
	return run(CMD,  [username]);
}

module.exports = removesystemuser;
