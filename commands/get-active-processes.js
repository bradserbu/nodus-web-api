'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function getactiveprocesses(user_token){
	return decryptToken(user_token)
		.then(user => readProcesses(user.username));
}

module.exports = getactiveprocesses;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const readProcesses = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/active_processes.json`, function(err, data) {
		if(err)
			throw err;

		var processes = JSON.parse(data).processes;
		resolve(processes);
	});
});
