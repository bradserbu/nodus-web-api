'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const request = require('./make-http-request');
const addProcess = require('./add-active-process');
const logActivity = require('./log-activity');
const fs = require('fs');

function submitjob(user_token, job, cluster) {
        return decryptToken(user_token)
                .then(result => {
                        const username = result.username;

			let jobcounter = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/jobs.json`)).jobs.length;
			const job_data = {
				'job_id': `job_${jobcounter}`,
				'job_name': job.job_name,
				'job_script': job.job_script,
				'job_data_files': job.job_data_files,
				'job_nodes_count': job.job_nodes_count,
				'job_cores_count': job.job_cores_count,
				'submit_time': new Date().toLocaleString(),
				'submit_cluster': cluster.id
			};

                        const postOptions = {
                        	method: 'post',
                        	body: job_data,
                        	json: true,
                        	url: `${cluster.api}/submitJob`
                      	};

                       	return request(postOptions)
                       		.then(result => {
                        		return updateJobStats(username, job_data)
						//.then(addProcess(user_token, `pid_${job_data.job_id}_exec`, `Executing ${job_data.job_name} (${job_data.job_id})`, [{'id':'exec_job_1','title':'Allocating Resources'}, {'id':'exec_job_2','title':'Running Job'}]))
                        			.then(_ => {
							const activity = {
								'type': 'job',
								'title': {
									'name': job_data.job_name + ' (ID: ' + job_data.job_id + ')',
									'icon': '/assets/images/test-drive/job.png', 
								},
								'message': ' was submitted to the workload.'
							};
							return logActivity(user_token, activity)
								.then(_ => job_data);
						});
				});
                });
}

module.exports = submitjob;


const decryptToken = (token) => new Promise(function(resolve, reject) {
        resolve(jwt.verify(token, config.app_secret));
});

const updateJobStats = (username, job_data) => new Promise(function(resolve, reject) {
        let jobstats = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/jobs.json`));
        jobstats.jobs.push(job_data);

        fs.writeFile(`/home/${username}/.nodus/jobs.json`, JSON.stringify(jobstats), function(err) {
                if(err)
                        throw err;
                resolve();
        });
});
