'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');

function logactivity(user_token, activity){
	return decryptToken(user_token)
		.then(user => saveActivityToLog(user.username, activity));
}

module.exports = logactivity;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const saveActivityToLog = (username, activity) => new Promise(function(resolve, reject) {
	const timestamp = new Date().toLocaleString();
	const logActivity = {
		'type': activity.type,
		'time': timestamp,
		'title': {
			'name': activity.title.name,
			'icon': activity.title.icon
		},
		'message': activity.message
	};

	fs.appendFile(`/home/${username}/.nodus/activities_history`, JSON.stringify(logActivity) + '\n', function(err) {
		if(err)
			throw err;
		resolve();
	});
});
