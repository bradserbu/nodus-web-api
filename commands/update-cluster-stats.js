'use strict'

const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const fs = require('fs');
const request = require('./make-http-request');
const destroyCluster = require('./destroy-cluster');

function updateclusterstats(user_token) {
        return decryptToken(user_token)
                .then(user => {
                        const username = user.username;
			
			return getClusters(username)
				.then(clusters => updateClusters(user_token, username, clusters));
                });
}

module.exports = updateclusterstats;

const decryptToken = (token) => new Promise(function(resolve, reject) {
        resolve(jwt.verify(token, config.app_secret));
});

const getClusters = (username) => new Promise(function(resolve, reject) {
	fs.readFile(`/home/${username}/.nodus/cluster_config.json`, function(err, data) {
		if(err)
			throw err;
	
		const clusters = JSON.parse(data).clusters;

                resolve(clusters);	
	});
});

const updateClusters = (user_token, username, clusters) => new Promise(function(resolve, reject) {
	clusters.forEach(function(cluster) {
		if(cluster.state == "Online") {
			const postOptions = {
				method: 'get',
				url: `${cluster.api}/getClusterWorkloadNodes`
			};
			request(postOptions)
				.then(result => {
					var data = JSON.parse(result).data;
					if(cluster.on_demand && data.queue.total == 0) {
						destroyCluster(user_token, cluster.provider, cluster.id, cluster.name);
					} else {
						updateWorkloadNodesHistory(username, cluster.name, data.queue.total, data.queue.running, data.queue.queued, data.queue.blocked, data.nodes.total, data.nodes.available, data.nodes.busy, data.nodes.down);
					}
				});
		}
		else {
			if(cluster.state != "Destroying") {
				updateWorkloadNodesHistory(username, cluster.name, 0, 0, 0, 0, 0, 0, 0, 0);
			}
		}
	});
	resolve();
});

const updateWorkloadNodesHistory = (username, cluster_name, workload_size, workload_running, workload_queued, workload_blocked, nodes_size, nodes_available, nodes_busy, nodes_down) => new Promise(function(resolve, reject) {
        const timestamp = new Date().toLocaleString();
        const currentState = {
                'timestamp': timestamp,
                'workload_size': workload_size,
		'workload_running': workload_running,
		'workload_queued': workload_queued,
		'workload_blocked': workload_blocked,
                'nodes_size': nodes_size,
		'nodes_available': nodes_available,
		'nodes_busy': nodes_busy,
		'nodes_down': nodes_down
        };

        fs.appendFile(`/home/${username}/.nodus/clusters/${cluster_name}/workload_nodes_history`, JSON.stringify(currentState) + '\n', function(err) {
                if(err)
                        throw err;
                resolve();
        });
});
