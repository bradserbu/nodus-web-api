'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const resizeCluster = "./scripts/resize-cluster.sh";
const logActivity = require('./log-activity');
const run = require('./run-shell-command');

function resizecluster(user_token, cluster_type, cluster_id, cluster_name, cluster_size)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return updateClusterState(username, cluster_id, cluster_size, "Resizing")
				.then(_ => {
					switch(cluster_type) {
						case "Amazon Web Services":
							return run(resizeCluster, ["aws", username, cluster_id, cluster_size])
								.then(_ => updateClusterState(username, cluster_id, cluster_size, "Online"))
								.then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Amazon Web Services.png',
                                                                                },
                                                                                'message': ' was resized.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
							break;
						case "Google Cloud":
                                                        return run(resizeCluster, ["google", username, cluster_id, cluster_size])
                                                                .then(_ => updateClusterState(username, cluster_id, cluster_size, "Online"))
								.then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Google Cloud.png',
                                                                                },
                                                                                'message': ' was resized.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });

                                                        break;
						case "Microsoft Azure":
                                                        return run(resizeCluster, ["azure", username, cluster_id, cluster_size])
                                                                .then(_ => updateClusterState(username, cluster_id, cluster_size, "Online"))
								.then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Microsoft Azure.png',
                                                                                },
                                                                                'message': ' was resized.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
                                                        break;
						case "Oracle Cloud":
                                                        return run(resizeCluster, ["oracle", username, cluster_id, cluster_size])
                                                                .then(_ => updateClusterState(username, cluster_id, cluster_size, "Online"))
								.then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Oracle Cloud.png',
                                                                                },
                                                                                'message': ' was resized.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
                                                        break;
						default:
							return;
					}
				});
		});
}

module.exports = resizecluster;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const updateClusterState  = (username, cluster_id, cluster_size, state) => new Promise(function(resolve, reject) {
        let cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));

        if(cluster_config.selected_cluster.id == cluster_id)
                cluster_config.selected_cluster = {};

        for(let i = 0; i < cluster_config.clusters.length; i++) {
                if(cluster_config.clusters[i].id == cluster_id) {
                        cluster_config.clusters[i].state = state;
			cluster_config.clusters[i].size = cluster_size;
                        break;
                }
        }

        fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;

                resolve();
        });
});
