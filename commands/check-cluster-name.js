'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');

function checkclustername(user_token, cluster_name)  {
	return decryptToken(user_token)
		.then(result => {
			const username = result.username;
			// CHECK IF CLUSTER WITH NAME ALREADY EXISTS
			return fs.existsSync(`/home/${username}/.nodus/clusters/${cluster_name}`);
		});
}

module.exports = checkclustername;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});
