'use strict'

const util = require('util');
const execa = require('execa');

function run(cmd, args) {
	args = args || [];
	return execa(cmd, args);
}

module.exports = run;
