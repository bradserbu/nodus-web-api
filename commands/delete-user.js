'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');

const removeUserFromSystem = require('./remove-user-from-system');


function deleteuser(token) {
	return decryptToken(token)
		.then(result => removeUserFromSystem(result.username));
}

module.exports = deleteuser;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});
