'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const destroyCluster = "./scripts/destroy-cluster.sh";
const logActivity = require('./log-activity');
const run = require('./run-shell-command');

function destroycluster(user_token, cluster_type, cluster_id, cluster_name)  {
	return decryptToken(user_token)
		.then(user => {
			const username = user.username;
			return updateClusterState(username, cluster_id)
				.then(_ => {
					switch(cluster_type) {
						case "Amazon Web Services":
							return run(destroyCluster, ["aws", username, cluster_id, cluster_name])
								.then(_ => removeFromClusterConfig(username, cluster_id))
								.then(_ => {
									const activity = {
                                                                		'type': 'cloud_provider',
                                                                		'title': {
                                                                        		'name': cluster_name,
                                                                        		'icon': '/assets/images/providers/Amazon Web Services.png',
                                                                		},
                                                                		'message': ' was destroyed.'
                                                        		};
                                                        		return logActivity(user_token, activity);
								});
							break;
						case "Google Cloud":
                                                        return run(destroyCluster, ["google", username, cluster_id, cluster_name])
                                                                .then(_ => removeFromClusterConfig(username, cluster_id))
                                                                .then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Google Cloud.png',
                                                                                },
                                                                                'message': ' was destroyed.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
                                                        break;
						case "Microsoft Azure":
                                                        return run(destroyCluster, ["azure", username, cluster_id, cluster_name])
                                                                .then(_ => removeFromClusterConfig(username, cluster_id))
                                                                .then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Microsoft Azure.png',
                                                                                },
                                                                                'message': ' was destroyed.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
                                                        break;
						case "Oracle Cloud":
							return run(destroyCluster, ["oracle", username, cluster_id, cluster_name])
                                                                .then(_ => removeFromClusterConfig(username, cluster_id))
                                                                .then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Oracle Cloud.png',
                                                                                },
                                                                                'message': ' was destroyed.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
                                                        break;
						case "Huawei Cloud":
                                                        return run(destroyCluster, ["huawei", username, cluster_id, cluster_name])
                                                                .then(_ => removeFromClusterConfig(username, cluster_id))
                                                                .then(_ => {
                                                                        const activity = {
                                                                                'type': 'cloud_provider',
                                                                                'title': {
                                                                                        'name': cluster_name,
                                                                                        'icon': '/assets/images/providers/Huawei Cloud.png',
                                                                                },
                                                                                'message': ' was destroyed.'
                                                                        };
                                                                        return logActivity(user_token, activity);
                                                                });
                                                        break;
						default:
							return;
					}
				});
		});
}

module.exports = destroycluster;


const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
});

const updateClusterState  = (username, cluster_id) => new Promise(function(resolve, reject) {
        let cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));

        if(cluster_config.selected_cluster.id == cluster_id)
                cluster_config.selected_cluster = {};

        for(let i = 0; i < cluster_config.clusters.length; i++) {
                if(cluster_config.clusters[i].id == cluster_id) {
                        cluster_config.clusters[i].state = "Destroying";
                        break;
                }
        }

        fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
                if(err)
                        throw err;

                resolve();
        });
});

const removeFromClusterConfig = (username, cluster_id) => new Promise(function(resolve, reject) {
	let cluster_config = JSON.parse(fs.readFileSync(`/home/${username}/.nodus/cluster_config.json`));

	if(cluster_config.selected_cluster.id == cluster_id)
		cluster_config.selected_cluster = {};

	for(let i = 0; i < cluster_config.clusters.length; i++) {
                if(cluster_config.clusters[i].id == cluster_id) {
                        cluster_config.clusters.splice(i, 1);
                        break;
                }
        }

	fs.writeFile(`/home/${username}/.nodus/cluster_config.json`, JSON.stringify(cluster_config), function(err) {
		if(err)
			throw err;

		resolve();
	});
});
