'use strict'

const request = require('request');

function makerequest(requestOptions) {
	return new Promise(function(resolve, reject) {
		request(requestOptions, function(err, res, body) {
			if(err)
				throw err;
			resolve(body);
		});
	});	
}

module.exports = makerequest;
