'use strict'

const config = require('./../config.json');
const jwt = require('jsonwebtoken');
const fs = require('fs');

function updateprofilepicture(user_token, image) {
	return decryptToken(user_token)
		.then(result => {
			const user = result;
			return saveProfilePicture(user.username, image)
				.then(_ => createUserToken(user))
				.then(result => saveUserToken(user.username, result));
		});
}

module.exports = updateprofilepicture;

const decryptToken = (token) => new Promise(function(resolve, reject) {
	resolve(jwt.verify(token, config.app_secret));
}); 

const createUserToken = (user, avatar) => new Promise(function(resolve, reject) {
	resolve(jwt.sign({
			username: user.username,
			avatar: `assets/images/avatars/${user.username}.png`,
			name: user.name,
			lastName: user.lastName,
			company: user.company,
			jobTitle: user.jobTitle,
			email: user.email,
			phone: user.phone
		}, config.app_secret, {
			expiresIn: "30d"
		})
	);
});

const saveUserToken = (username, user_token) => new Promise(function(resolve, reject) {
        fs.writeFile(`/home/${username}/.nodus/token`, user_token.toString(), 'utf8', function(err) {
                if(err)
                        throw err;
                resolve(user_token);
        });
});

const saveProfilePicture = (username, image) => new Promise(function(resolve, reject) {
	//resolve(file);
	var data = image.replace(/^data:image\/\w+;base64,/, "");
	var buf = new Buffer(data, 'base64');
	fs.writeFile(`/vagrant/saas-ui/src/assets/images/avatars/${username}.png`, buf, function(err) {
		if(err)
			throw err;
		resolve();
	});

});
