#!/bin/bash

PROVIDER=$1
USERNAME=$2
CLUSTERID=$3
CLUSTERNAME=$4
CLUSTERSIZE=$5
HEADNODESIZE=$6
COMPUTENODESIZE=$7
NODECOUNT=$((CLUSTERSIZE-1))
TIMESTAMP=$(date)

cd /home/$USERNAME
sudo chown -R $USERNAME:$USERNAME .nodus
sudo chown -R $USERNAME:$USERNAME .ssh
cp /NODUS/nodus-cluster/$PROVIDER-cluster.json /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
jq --arg clusterid $CLUSTERID '.name = "\($clusterid)"' /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json > /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json && mv /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
jq --arg nodecount $NODECOUNT '.node.count = "\($nodecount)"' /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json > /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json && mv /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
jq --arg headnodesize $HEADNODESIZE '.server.size = "\($headnodesize)"' /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json > /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json && mv /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
jq --arg computenodesize $COMPUTENODESIZE '.node.size = "\($computenodesize)"' /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json > /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json && mv /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
sudo su $USERNAME << EOF
ln -s /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/ /home/$USERNAME/.nodus/clusters/"$CLUSTERNAME"
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "***CLUSTER DEPLOY****" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "***TIMESTAMP: $TIMESTAMP****" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
(bash /NODUS/nodus-cluster/deploy.sh /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json 2>&1) | tee -a /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
EOF
