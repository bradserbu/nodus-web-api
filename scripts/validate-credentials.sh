#!/bin/bash

uid=`id -u`

if [ $uid -ne 0 ]; then 
    echo "You must be root to run this"
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "You must supply a username to check - ($# supplied)"
    exit 1
fi

USERNAME=$1
PASSWORD=$2

salt=`grep $USERNAME /etc/shadow | awk -F: ' {print substr($2,4,8)}'`

if [ "$salt" != "" ]; then
        newpass=`echo $PASSWORD | openssl passwd -1 -salt $salt -stdin`
        grep $USERNAME  /etc/shadow | grep -q  $newpass && exit 0 || exit 1

fi
