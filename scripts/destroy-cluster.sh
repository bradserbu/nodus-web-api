#!/bin/bash

PROVIDER=$1
USERNAME=$2
CLUSTERID=$3
CLUSTERNAME=$4

cd /home/$USERNAME
sudo su $USERNAME << EOF
rm -f /home/$USERNAME/.nodus/clusters/"$CLUSTERNAME"
bash /NODUS/nodus-cluster/destroy.sh /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
EOF
