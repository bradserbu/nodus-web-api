#!/bin/bash

PROVIDER=$1
USERNAME=$2
CLUSTERID=$3

sudo su $USERNAME << EOF
(bash /NODUS/nodus-cluster/cluster-online.sh /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json 2>&1) | tee -a /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
EOF
