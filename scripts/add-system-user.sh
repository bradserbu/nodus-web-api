#!/bin/bash

USERNAME=$1

### CHANGE DIRECTORY TO SCRIPT LOCATION ###
SCRIPT_DIR="$(dirname "$0")"
cd $SCRIPT_DIR


### ADD USER ###
sudo useradd -p $(openssl passwd -1 $2) $USERNAME

### COPY HOME DIRECTORY TEMPLATE ###
sudo cp -r ./user/.nodus /home/$USERNAME

### SSH KEYGEN ###
sudo mkdir /home/$USERNAME/.ssh
sudo ssh-keygen -f /home/$USERNAME/.ssh/id_rsa -t rsa -N ''
