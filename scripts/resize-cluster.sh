#!/bin/bash

PROVIDER=$1
USERNAME=$2
CLUSTERID=$3
CLUSTERSIZE=$4
NODECOUNT=$((CLUSTERSIZE-1))

cd /home/$USERNAME
sudo chown -R $USERNAME:$USERNAME .nodus
sudo chown -R $USERNAME:$USERNAME .ssh
jq --arg nodecount $NODECOUNT '.node.count = "\($nodecount)"' /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json > /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json && mv /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/tmp.json /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
sudo su $USERNAME << EOF
bash /NODUS/nodus-cluster/deploy.sh /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
EOF
