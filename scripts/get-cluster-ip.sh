#!/bin/bash

PROVIDER=$1
USERNAME=$2
CLUSTERID=$3

cd /home/$USERNAME
sudo su $USERNAME << EOF
bash /NODUS/nodus-cluster/public-ip.sh /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json
EOF
