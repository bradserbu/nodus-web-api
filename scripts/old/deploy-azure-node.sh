#!/bin/bash

USERNAME=$1
cd /home/$USERNAME
sudo -H -u $USERNAME bash -c 'bash /NODUS/nodus-azure-cluster/add-node.sh /home/$USERNAME/.nodus/azure-cluster.json'
