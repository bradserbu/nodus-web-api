#!/bin/bash

USERNAME=$1
NODE=$2
echo $NODE
cd /home/$USERNAME
sudo -H -u $USERNAME bash -c "bash /NODUS/nodus-azure-cluster/remove-node.sh /home/$USERNAME/.nodus/azure-cluster.json $NODE"
