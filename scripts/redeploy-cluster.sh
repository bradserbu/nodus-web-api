#!/bin/bash

PROVIDER=$1
USERNAME=$2
CLUSTERID=$3
TIMESTAMP=$(date)

cd /home/$USERNAME
sudo chown -R $USERNAME:$USERNAME .nodus
sudo chown -R $USERNAME:$USERNAME .ssh
sudo su $USERNAME << EOF
echo "" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "***CLUSTER RE-DEPLOY****" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "***TIMESTAMP: $TIMESTAMP****" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
echo "********************************************************************************" >> /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
(bash /NODUS/nodus-cluster/deploy.sh /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/cluster.json 2>&1) | tee -a /home/$USERNAME/.nodus/$PROVIDER/$CLUSTERID/deploy_logs
EOF
